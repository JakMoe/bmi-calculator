﻿using System;

namespace BMI_Calculator
{
    class Program
    {
        public static void RunProgram()
        {
            Console.WriteLine("Welcome to the BMI-calculator, please enter your height in cm: ");
            string userHeight = Console.ReadLine();
            Console.WriteLine("Enter your weight in kgs: ");
            string userWeight = Console.ReadLine();
            float BMI = CalculateBMI(userHeight, userWeight);
            Console.WriteLine("Your BMI is " + BMI);
              
        }
        public static float CalculateBMI(string height, string weight)
        {
            float tempHeight = ((float)Int32.Parse(height))/100;
            float tempWeight = (float)Int32.Parse(weight);
            float BMI = tempWeight/(tempHeight*tempHeight);
            return BMI;
        }
        static void Main(string[] args)
        {
            RunProgram();        }
    }
}
